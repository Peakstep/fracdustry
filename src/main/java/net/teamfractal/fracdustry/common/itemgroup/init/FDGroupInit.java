package net.teamfractal.fracdustry.common.itemgroup.init;

import io.wispforest.owo.itemgroup.Icon;
import io.wispforest.owo.itemgroup.OwoItemGroup;
import net.fabricmc.fabric.api.tag.TagFactory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.tag.Tag;
import net.minecraft.util.Identifier;
import net.teamfractal.fracdustry.common.item.init.FDItems;

public class FDGroupInit
{
    //通过tag向创造物品栏添加物品
    public static final Tag<Item> FD_MATERIAL_CONTENT = TagFactory.ITEM.create(new Identifier("fracdustry", "material_content"));
    public static final Tag<Item> FD_MACHINERY_CONTENT = TagFactory.ITEM.create(new Identifier("fracdustry", "machinery_content"));
    public static final Tag<Item> FD_TOOLS_CONTENT = TagFactory.ITEM.create(new Identifier("fracdustry", "tools_content"));
    public static final Tag<Item> FD_RESEARCH_CONTENT = TagFactory.ITEM.create(new Identifier("fracdustry", "research_content"));
    public static final OwoItemGroup FD_MAIN = new OwoItemGroup(new Identifier("fracdustry","main")) {
        @Override
        protected void setup() {
            keepStaticTitle();

            addTab(Icon.of(FDItems.STEEL_INGOT), "material", FD_MATERIAL_CONTENT);
            addTab(Icon.of(Items.CRAFTING_TABLE), "machinery", FD_MACHINERY_CONTENT);
            addTab(Icon.of(Items.IRON_PICKAXE), "tools", FD_TOOLS_CONTENT);
            addTab(Icon.of(Items.DIAMOND), "research", FD_RESEARCH_CONTENT);
        }

        @Override
        public ItemStack createIcon() {
            return new ItemStack(Items.CRAFTING_TABLE);
        }
    };
}
