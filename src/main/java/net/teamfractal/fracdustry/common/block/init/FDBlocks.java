package net.teamfractal.fracdustry.common.block.init;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.*;
import net.minecraft.fluid.FlowableFluid;
import net.minecraft.fluid.FluidState;
import net.minecraft.item.Item;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.teamfractal.fracdustry.common.fluid.init.FDPetroleumFluid;

public class FDBlocks
{
    /* 创建上层矿石 */
    public static final Block BAUXITE_ORE;
    public static final Block CASSITERITE_ORE;
    public static final Block ILMENITE_ORE;
    public static final Block RARE_EARTH_ORE;
    public static final Block SPODUMENE_ORE;

    /* 创建深层矿石 */
    public static final Block DEEPSLATE_BAUXITE_ORE;
    public static final Block DEEPSLATE_CASSITERITE_ORE;
    public static final Block DEEPSLATE_ILMENITE_ORE;
    public static final Block DEEPSLATE_RARE_EARTH_ORE;
    public static final Block DEEPSLATE_SPODUMENE_ORE;

    public static FlowableFluid PETROLEUM_STILL;
    public static FlowableFluid PETROLEUM_FLOW;
    public static Block PETROLEUM;

    static
    {
        /* 初始化上层矿石 */
        BAUXITE_ORE = new OreBlock(AbstractBlock.Settings.of(Material.STONE)
                .resistance(2.0f).sounds(BlockSoundGroup.STONE)
                .strength(2.0f).requiresTool());
        CASSITERITE_ORE = new OreBlock(AbstractBlock.Settings.of(Material.STONE)
                .resistance(2.0f).sounds(BlockSoundGroup.STONE)
                .strength(2.0f).requiresTool());
        ILMENITE_ORE = new OreBlock(AbstractBlock.Settings.of(Material.STONE)
                .resistance(2.0f).sounds(BlockSoundGroup.STONE)
                .strength(2.0f).requiresTool());
        RARE_EARTH_ORE = new OreBlock(AbstractBlock.Settings.of(Material.STONE)
                .resistance(2.0f).sounds(BlockSoundGroup.STONE)
                .strength(2.0f).requiresTool());
        SPODUMENE_ORE = new OreBlock(AbstractBlock.Settings.of(Material.STONE)
                .resistance(2.0f).sounds(BlockSoundGroup.STONE)
                .strength(2.0f).requiresTool());

        /* 初始化深层矿石 */
        DEEPSLATE_BAUXITE_ORE = new OreBlock(AbstractBlock.Settings.of(Material.STONE)
                .resistance(3.0f).sounds(BlockSoundGroup.STONE)
                .strength(2.0f).requiresTool());
        DEEPSLATE_CASSITERITE_ORE = new OreBlock(AbstractBlock.Settings.of(Material.STONE)
                .resistance(3.0f).sounds(BlockSoundGroup.STONE)
                .strength(2.0f).requiresTool());
        DEEPSLATE_ILMENITE_ORE = new OreBlock(AbstractBlock.Settings.of(Material.STONE)
                .resistance(3.0f).sounds(BlockSoundGroup.STONE)
                .strength(2.0f).requiresTool());
        DEEPSLATE_RARE_EARTH_ORE = new OreBlock(AbstractBlock.Settings.of(Material.STONE)
                .resistance(3.0f).sounds(BlockSoundGroup.STONE)
                .strength(2.0f).requiresTool());
        DEEPSLATE_SPODUMENE_ORE = new OreBlock(AbstractBlock.Settings.of(Material.STONE)
                .resistance(3.0f).sounds(BlockSoundGroup.STONE)
                .strength(2.0f).requiresTool());
    }

    /* 注册方块 */
    public static void registerBlocks()
    {
        /* 注册上层矿石 */
        Registry.register(Registry.BLOCK, new Identifier("fracdustry", "bauxite_ore"), BAUXITE_ORE);
        Registry.register(Registry.BLOCK, new Identifier("fracdustry", "cassiterite_ore"), CASSITERITE_ORE);
        Registry.register(Registry.BLOCK, new Identifier("fracdustry", "ilmenite_ore"), ILMENITE_ORE);
        Registry.register(Registry.BLOCK, new Identifier("fracdustry", "rare_earth_ore"), RARE_EARTH_ORE);
        Registry.register(Registry.BLOCK, new Identifier("fracdustry", "spodumene_ore"), SPODUMENE_ORE);

        /* 注册深层矿石 */
        Registry.register(Registry.BLOCK, new Identifier("fracdustry", "deepslate_bauxite_ore"), DEEPSLATE_BAUXITE_ORE);
        Registry.register(Registry.BLOCK, new Identifier("fracdustry", "deepslate_cassiterite_ore"), DEEPSLATE_CASSITERITE_ORE);
        Registry.register(Registry.BLOCK, new Identifier("fracdustry", "deepslate_ilmenite_ore"), DEEPSLATE_ILMENITE_ORE);
        Registry.register(Registry.BLOCK, new Identifier("fracdustry", "deepslate_rare_earth_ore"), DEEPSLATE_RARE_EARTH_ORE);
        Registry.register(Registry.BLOCK, new Identifier("fracdustry", "deepslate_spodumene_ore"), DEEPSLATE_SPODUMENE_ORE);

        PETROLEUM_STILL = Registry.register(Registry.FLUID, new Identifier("fracdustry", "petroleum_still"), new FDPetroleumFluid.Still());
        PETROLEUM_FLOW = Registry.register(Registry.FLUID, new Identifier("fracdustry", "petroleum_flow"), new FDPetroleumFluid.Flowing());
        PETROLEUM = Registry.register(Registry.BLOCK, new Identifier("fracdustry", "petroleum"), new FluidBlock(PETROLEUM_STILL, FabricBlockSettings.copy(Blocks.WATER)){});
    }
}
