package net.teamfractal.fracdustry.common.fluid.init;

import net.minecraft.block.BlockState;
import net.minecraft.fluid.Fluid;
import net.minecraft.fluid.FluidState;
import net.minecraft.item.Item;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.Properties;
import net.teamfractal.fracdustry.common.fluid.FDBasicFluid;

import static net.teamfractal.fracdustry.common.block.init.FDBlocks.*;
import static net.teamfractal.fracdustry.common.item.init.FDItems.PETROLEUM_BUCKET;

public abstract class FDPetroleumFluid extends FDBasicFluid {
    @Override
    public Fluid getStill() {
        return PETROLEUM_STILL;
    }

    @Override
    public Fluid getFlowing() {
        return PETROLEUM_FLOW;
    }

    @Override
    public Item getBucketItem() {
        return PETROLEUM_BUCKET;
    }

    @Override
    protected BlockState toBlockState(FluidState fluidState) {
        return PETROLEUM.getDefaultState().with(Properties.LEVEL_15, getBlockStateLevel(fluidState));
    }

    public static class Flowing extends FDPetroleumFluid {
        @Override
        protected void appendProperties(StateManager.Builder<Fluid, FluidState> builder) {
            super.appendProperties(builder);
            builder.add(LEVEL);
        }

        @Override
        public int getLevel(FluidState fluidState) {
            return fluidState.get(LEVEL);
        }

        @Override
        public boolean isStill(FluidState fluidState) {
            return false;
        }
    }

    public static class Still extends FDPetroleumFluid {
        @Override
        public int getLevel(FluidState fluidState) {
            return 8;
        }

        @Override
        public boolean isStill(FluidState fluidState) {
            return true;
        }
    }
}
