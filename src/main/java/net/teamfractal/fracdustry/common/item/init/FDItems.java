package net.teamfractal.fracdustry.common.item.init;

import net.minecraft.item.BlockItem;
import net.minecraft.item.BucketItem;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.teamfractal.fracdustry.common.block.init.FDBlocks;

import static net.teamfractal.fracdustry.common.block.init.FDBlocks.PETROLEUM_STILL;

public class FDItems
{
    /* 创建矿物粉 */
    public static final Item COPPER_DUST;
    public static final Item IRON_DUST;
    public static final Item GOLD_DUST;
    public static final Item ALUMINUM_DUST;
    public static final Item BRONZE_DUST;
    public static final Item LITHIUM_DUST;
    public static final Item STEEL_DUST;
    public static final Item TIN_DUST;
    public static final Item TITANIUM_DUST;

    /* 创建矿物锭 */
    public static final Item ALUMINUM_INGOT;
    public static final Item BRONZE_INGOT;
    public static final Item LITHIUM_INGOT;
    public static final Item STEEL_INGOT;
    public static final Item TIN_INGOT;
    public static final Item TITANIUM_INGOT;

    /* 创建矿物板 */
    public static final Item ALUMINUM_PLATE;
    public static final Item BRONZE_PLATE;
    public static final Item STEEL_PLATE;
    public static final Item TIN_PLATE;
    public static final Item TITANIUM_PLATE;

    /* 创建矿物物品 */
    public static final Item BAUXITE_ORE;
    public static final Item CASSITERITE_ORE;
    public static final Item ILMENITE_ORE;
    public static final Item RARE_EARTH_ORE;
    public static final Item SPODUMENE_ORE;

    /* 创建深层矿物物品 */
    public static final Item DEEPSLATE_BAUXITE_ORE;
    public static final Item DEEPSLATE_CASSITERITE_ORE;
    public static final Item DEEPSLATE_ILMENITE_ORE;
    public static final Item DEEPSLATE_RARE_EARTH_ORE;
    public static final Item DEEPSLATE_SPODUMENE_ORE;

    /* 创建原油桶 */
    public static Item PETROLEUM_BUCKET;
    

    static {
        /* 初始化矿物粉 */
        COPPER_DUST = new Item(new Item.Settings());
        IRON_DUST = new Item(new Item.Settings());
        GOLD_DUST = new Item(new Item.Settings());
        ALUMINUM_DUST = new Item(new Item.Settings());
        BRONZE_DUST = new Item(new Item.Settings());
        LITHIUM_DUST = new Item(new Item.Settings());
        STEEL_DUST = new Item(new Item.Settings());
        TIN_DUST = new Item(new Item.Settings());
        TITANIUM_DUST = new Item(new Item.Settings());

        /* 初始化矿物锭 */
        ALUMINUM_INGOT = new Item(new Item.Settings());
        BRONZE_INGOT = new Item(new Item.Settings());
        LITHIUM_INGOT = new Item(new Item.Settings());
        STEEL_INGOT = new Item(new Item.Settings());
        TIN_INGOT = new Item(new Item.Settings());
        TITANIUM_INGOT = new Item(new Item.Settings());

        /* 初始化矿物板 */
        ALUMINUM_PLATE = new Item(new Item.Settings());
        BRONZE_PLATE = new Item(new Item.Settings());
        STEEL_PLATE = new Item(new Item.Settings());
        TIN_PLATE = new Item(new Item.Settings());
        TITANIUM_PLATE = new Item(new Item.Settings());

        /* 初始化上层矿物物品 */
        BAUXITE_ORE = new BlockItem(FDBlocks.BAUXITE_ORE, new Item.Settings());
        CASSITERITE_ORE = new BlockItem(FDBlocks.CASSITERITE_ORE, new Item.Settings());
        ILMENITE_ORE = new BlockItem(FDBlocks.ILMENITE_ORE, new Item.Settings());
        RARE_EARTH_ORE = new BlockItem(FDBlocks.RARE_EARTH_ORE, new Item.Settings());
        SPODUMENE_ORE = new BlockItem(FDBlocks.SPODUMENE_ORE, new Item.Settings());

        /* 初始化深层矿物物品 */
        DEEPSLATE_BAUXITE_ORE = new BlockItem(FDBlocks.DEEPSLATE_BAUXITE_ORE, new Item.Settings());
        DEEPSLATE_CASSITERITE_ORE = new BlockItem(FDBlocks.DEEPSLATE_CASSITERITE_ORE, new Item.Settings());
        DEEPSLATE_ILMENITE_ORE = new BlockItem(FDBlocks.DEEPSLATE_ILMENITE_ORE, new Item.Settings());
        DEEPSLATE_RARE_EARTH_ORE = new BlockItem(FDBlocks.DEEPSLATE_RARE_EARTH_ORE, new Item.Settings());
        DEEPSLATE_SPODUMENE_ORE = new BlockItem(FDBlocks.DEEPSLATE_SPODUMENE_ORE, new Item.Settings());
    }

    /** 注册物品 */
    public static void registerItems()
    {
        /* 注册矿物粉 */
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "copper_dust"), COPPER_DUST);
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "iron_dust"), IRON_DUST);
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "gold_dust"), GOLD_DUST);
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "aluminum_dust"), ALUMINUM_DUST);
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "bronze_dust"), BRONZE_DUST);
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "lithium_dust"), LITHIUM_DUST);
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "steel_dust"), STEEL_DUST);
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "tin_dust"), TIN_DUST);
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "titanium_dust"), TITANIUM_DUST);

        /* 注册矿物锭 */
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "aluminum_ingot"), ALUMINUM_INGOT);
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "bronze_ingot"), BRONZE_INGOT);
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "lithium_ingot"), LITHIUM_INGOT);
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "steel_ingot"), STEEL_INGOT);
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "tin_ingot"), TIN_INGOT);
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "titanium_ingot"), TITANIUM_INGOT);

        /* 注册矿物板 */
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "aluminum_plate"), ALUMINUM_PLATE);
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "bronze_plate"), BRONZE_PLATE);
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "steel_plate"), STEEL_PLATE);
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "tin_plate"), TIN_PLATE);
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "titanium_plate"), TITANIUM_PLATE);

        /* 注册上层矿物物品 */
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "bauxite_ore"), BAUXITE_ORE);
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "cassiterite_ore"), CASSITERITE_ORE);
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "ilmenite_ore"), ILMENITE_ORE);
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "rare_earth_ore"), RARE_EARTH_ORE);
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "spodumene_ore"), SPODUMENE_ORE);

        /* 注册深层矿物物品 */
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "deepslate_bauxite_ore"), DEEPSLATE_BAUXITE_ORE);
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "deepslate_cassiterite_ore"), DEEPSLATE_CASSITERITE_ORE);
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "deepslate_ilmenite_ore"), DEEPSLATE_ILMENITE_ORE);
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "deepslate_rare_earth_ore"), DEEPSLATE_RARE_EARTH_ORE);
        Registry.register(Registry.ITEM, new Identifier("fracdustry", "deepslate_spodumene_ore"), DEEPSLATE_SPODUMENE_ORE);

        /* 注册原油桶 */
        PETROLEUM_BUCKET = Registry.register(Registry.ITEM, new Identifier("fracdustry", "petroleum_bucket"), new BucketItem(PETROLEUM_STILL, new Item.Settings().recipeRemainder(Items.BUCKET).maxCount(1)));
    }
}
