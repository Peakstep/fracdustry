package net.teamfractal.fracdustry.common.world.gen;

import net.fabricmc.fabric.api.biome.v1.BiomeModifications;
import net.fabricmc.fabric.api.biome.v1.BiomeSelectors;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.BuiltinRegistries;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.gen.GenerationStep;
import net.minecraft.world.gen.YOffset;
import net.minecraft.world.gen.decorator.*;
import net.minecraft.world.gen.feature.*;
import net.teamfractal.fracdustry.common.block.init.FDBlocks;

import java.util.List;

public class FDOreGeneration {

    /*矿物生成配置*/
    public static List<OreFeatureConfig.Target> BAUXITE_ORES;
    public static List<OreFeatureConfig.Target> DEEPSLATE_BAUXITE_ORES;
    public static List<OreFeatureConfig.Target> CASSITERITE_ORES;
    public static List<OreFeatureConfig.Target> DEEPSLATE_CASSITERITE_ORES;
    public static List<OreFeatureConfig.Target> ILMENITE_ORES;
    public static List<OreFeatureConfig.Target> DEEPSLATE_ILMENITE_ORES;
    public static List<OreFeatureConfig.Target> RARE_EARTH_ORES;
    public static List<OreFeatureConfig.Target> DEEPSLATE_RARE_EARTH_ORES;
    public static List<OreFeatureConfig.Target> SPODUMENE_ORES;
    public static List<OreFeatureConfig.Target> DEEPSLATE_SPODUMENE_ORES;

    /*矿物生成结构*/
    public static ConfiguredFeature<?, ?> ORE_BAUXITE;
    public static ConfiguredFeature<?, ?> ORE_DEEPSLATE_BAUXITE;
    public static ConfiguredFeature<?, ?> ORE_CASSITERITE;
    public static ConfiguredFeature<?, ?> ORE_DEEPSLATE_CASSITERITE;
    public static ConfiguredFeature<?, ?> ORE_ILMENITE;
    public static ConfiguredFeature<?, ?> ORE_DEEPSLATE_ILMENITE;
    public static ConfiguredFeature<?, ?> ORE_RARE_EARTH;
    public static ConfiguredFeature<?, ?> ORE_DEEPSLATE_RARE_EARTH;
    public static ConfiguredFeature<?, ?> ORE_SPODUMENE;
    public static ConfiguredFeature<?, ?> ORE_DEEPSLATE_SPODUMENE;

    /*最终版*/
    public static PlacedFeature ORE_BAUXITE_PLACED;
    public static PlacedFeature ORE_DEEPSLATE_BAUXITE_PLACED;
    public static PlacedFeature ORE_CASSITERITE_PLACED;
    public static PlacedFeature ORE_DEEPSLATE_CASSITERITE_PLACED;
    public static PlacedFeature ORE_ILMENITE_PLACED;
    public static PlacedFeature ORE_DEEPSLATE_ILMENITE_PLACED;
    public static PlacedFeature ORE_RARE_EARTH_PLACED;
    public static PlacedFeature ORE_DEEPSLATE_RARE_EARTH_PLACED;
    public static PlacedFeature ORE_SPODUMENE_PLACED;
    public static PlacedFeature ORE_DEEPSLATE_SPODUMENE_PLACED;


    /*矿物生成结构ID*/
    public static Identifier ORE_BAUXITE_PLACED_ID;
    public static Identifier ORE_DEEPSLATE_BAUXITE_PLACED_ID;
    public static Identifier ORE_CASSITERITE_PLACED_ID;
    public static Identifier ORE_DEEPSLATE_CASSITERITE_PLACED_ID;
    public static Identifier ORE_ILMENITE_PLACED_ID;
    public static Identifier ORE_DEEPSLATE_ILMENITE_PLACED_ID;
    public static Identifier ORE_RARE_EARTH_PLACED_ID;
    public static Identifier ORE_DEEPSLATE_RARE_EARTH_PLACED_ID;
    public static Identifier ORE_SPODUMENE_PLACED_ID;
    public static Identifier ORE_DEEPSLATE_SPODUMENE_PLACED_ID;


    static {

        /*初始化矿物生成配置*/
        BAUXITE_ORES = List.of(OreFeatureConfig.createTarget(OreConfiguredFeatures.STONE_ORE_REPLACEABLES, FDBlocks.BAUXITE_ORE.getDefaultState()));
        DEEPSLATE_BAUXITE_ORES = List.of(OreFeatureConfig.createTarget(OreConfiguredFeatures.DEEPSLATE_ORE_REPLACEABLES, FDBlocks.DEEPSLATE_BAUXITE_ORE.getDefaultState()));
        CASSITERITE_ORES = List.of(OreFeatureConfig.createTarget(OreConfiguredFeatures.STONE_ORE_REPLACEABLES, FDBlocks.CASSITERITE_ORE.getDefaultState()));
        DEEPSLATE_CASSITERITE_ORES = List.of(OreFeatureConfig.createTarget(OreConfiguredFeatures.DEEPSLATE_ORE_REPLACEABLES, FDBlocks.DEEPSLATE_CASSITERITE_ORE.getDefaultState()));
        ILMENITE_ORES = List.of(OreFeatureConfig.createTarget(OreConfiguredFeatures.STONE_ORE_REPLACEABLES, FDBlocks.ILMENITE_ORE.getDefaultState()));
        DEEPSLATE_ILMENITE_ORES = List.of(OreFeatureConfig.createTarget(OreConfiguredFeatures.DEEPSLATE_ORE_REPLACEABLES, FDBlocks.DEEPSLATE_ILMENITE_ORE.getDefaultState()));
        RARE_EARTH_ORES = List.of(OreFeatureConfig.createTarget(OreConfiguredFeatures.STONE_ORE_REPLACEABLES, FDBlocks.RARE_EARTH_ORE.getDefaultState()));
        DEEPSLATE_RARE_EARTH_ORES = List.of(OreFeatureConfig.createTarget(OreConfiguredFeatures.DEEPSLATE_ORE_REPLACEABLES, FDBlocks.DEEPSLATE_RARE_EARTH_ORE.getDefaultState()));
        SPODUMENE_ORES = List.of(OreFeatureConfig.createTarget(OreConfiguredFeatures.STONE_ORE_REPLACEABLES, FDBlocks.CASSITERITE_ORE.getDefaultState()));
        DEEPSLATE_SPODUMENE_ORES = List.of(OreFeatureConfig.createTarget(OreConfiguredFeatures.DEEPSLATE_ORE_REPLACEABLES, FDBlocks.DEEPSLATE_CASSITERITE_ORE.getDefaultState()));

        /*初始化矿物生成结构*/
        ORE_BAUXITE = Feature.ORE.configure(new OreFeatureConfig(BAUXITE_ORES, 6));
        ORE_DEEPSLATE_BAUXITE = Feature.ORE.configure(new OreFeatureConfig(DEEPSLATE_BAUXITE_ORES, 4));
        ORE_CASSITERITE = Feature.ORE.configure(new OreFeatureConfig(CASSITERITE_ORES, 8));
        ORE_DEEPSLATE_CASSITERITE = Feature.ORE.configure(new OreFeatureConfig(DEEPSLATE_CASSITERITE_ORES, 6));
        ORE_ILMENITE = Feature.ORE.configure(new OreFeatureConfig(ILMENITE_ORES, 4));
        ORE_DEEPSLATE_ILMENITE = Feature.ORE.configure(new OreFeatureConfig(DEEPSLATE_ILMENITE_ORES, 4));
        ORE_RARE_EARTH = Feature.ORE.configure(new OreFeatureConfig(RARE_EARTH_ORES, 4));
        ORE_DEEPSLATE_RARE_EARTH = Feature.ORE.configure(new OreFeatureConfig(DEEPSLATE_RARE_EARTH_ORES, 4));
        ORE_SPODUMENE = Feature.ORE.configure(new OreFeatureConfig(SPODUMENE_ORES, 10));
        ORE_DEEPSLATE_SPODUMENE = Feature.ORE.configure(new OreFeatureConfig(DEEPSLATE_SPODUMENE_ORES, 8));

        /*完善版矿物生成结构placedFeature*/
        ORE_BAUXITE_PLACED = ORE_BAUXITE.withPlacement(modifiersWithCount(11, HeightRangePlacementModifier.trapezoid(YOffset.fixed(32), YOffset.fixed(256))));
        ORE_DEEPSLATE_BAUXITE_PLACED = ORE_DEEPSLATE_BAUXITE.withPlacement(modifiersWithCount(11, HeightRangePlacementModifier.trapezoid(YOffset.fixed(-64), YOffset.fixed(32))));
        ORE_CASSITERITE_PLACED = ORE_CASSITERITE.withPlacement(modifiersWithCount(13, HeightRangePlacementModifier.trapezoid(YOffset.fixed(32), YOffset.fixed(256))));
        ORE_DEEPSLATE_CASSITERITE_PLACED = ORE_DEEPSLATE_CASSITERITE.withPlacement(modifiersWithCount(13, HeightRangePlacementModifier.trapezoid(YOffset.fixed(-64), YOffset.fixed(32))));
        ORE_ILMENITE_PLACED = ORE_ILMENITE.withPlacement(modifiersWithCount(12, HeightRangePlacementModifier.trapezoid(YOffset.fixed(32), YOffset.fixed(256))));
        ORE_DEEPSLATE_ILMENITE_PLACED = ORE_DEEPSLATE_ILMENITE.withPlacement(modifiersWithCount(12, HeightRangePlacementModifier.trapezoid(YOffset.fixed(-64), YOffset.fixed(32))));
        ORE_RARE_EARTH_PLACED = ORE_RARE_EARTH.withPlacement(modifiersWithCount(2, HeightRangePlacementModifier.trapezoid(YOffset.fixed(32), YOffset.fixed(256))));
        ORE_DEEPSLATE_RARE_EARTH_PLACED = ORE_DEEPSLATE_RARE_EARTH.withPlacement(modifiersWithCount(3, HeightRangePlacementModifier.trapezoid(YOffset.fixed(0), YOffset.fixed(16))));
        ORE_SPODUMENE_PLACED = ORE_SPODUMENE.withPlacement(modifiersWithCount(15, HeightRangePlacementModifier.trapezoid(YOffset.fixed(32), YOffset.fixed(256))));
        ORE_DEEPSLATE_SPODUMENE_PLACED = ORE_DEEPSLATE_SPODUMENE.withPlacement(modifiersWithCount(15, HeightRangePlacementModifier.trapezoid(YOffset.fixed(-64), YOffset.fixed(32))));

        /*ID*/
        ORE_BAUXITE_PLACED_ID = new Identifier("fracdustry", "ore_bauxite_placed");
        ORE_DEEPSLATE_BAUXITE_PLACED_ID = new Identifier("fracdustry", "ore_deepslate_bauxite_placed");
        ORE_CASSITERITE_PLACED_ID = new Identifier("fracdustry", "ore_cassiterite_placed");
        ORE_DEEPSLATE_CASSITERITE_PLACED_ID = new Identifier("fracdustry", "ore_deepslate_cassiterite_placed");
        ORE_ILMENITE_PLACED_ID = new Identifier("fracdustry", "ore_ilmenite_placed");
        ORE_DEEPSLATE_ILMENITE_PLACED_ID = new Identifier("fracdustry", "ore_deepslate_ilmenite_placed");
        ORE_RARE_EARTH_PLACED_ID = new Identifier("fracdustry", "ore_rare_earth_placed");
        ORE_DEEPSLATE_RARE_EARTH_PLACED_ID = new Identifier("fracdustry", "ore_deepslate_rare_earth_placed");
        ORE_SPODUMENE_PLACED_ID = new Identifier("fracdustry", "ore_spodumene_placed");
        ORE_DEEPSLATE_SPODUMENE_PLACED_ID = new Identifier("fracdustry", "ore_deepslate_spodumene_placed");
    }

    /*矿物结构生成，在Main中调用*/
    public static void registerOreGenerations(){

        /*矿物结构注册*/
        Registry.register(BuiltinRegistries.CONFIGURED_FEATURE, new Identifier("fracdustry", "ore_bauxite"), ORE_BAUXITE);
        Registry.register(BuiltinRegistries.CONFIGURED_FEATURE, new Identifier("fracdustry", "ore_deepslate_bauxite"), ORE_DEEPSLATE_BAUXITE);
        Registry.register(BuiltinRegistries.CONFIGURED_FEATURE, new Identifier("fracdustry", "ore_cassiterite"), ORE_CASSITERITE);
        Registry.register(BuiltinRegistries.CONFIGURED_FEATURE, new Identifier("fracdustry", "ore_deepslate_cassiterite"), ORE_DEEPSLATE_CASSITERITE);
        Registry.register(BuiltinRegistries.CONFIGURED_FEATURE, new Identifier("fracdustry", "ore_ilmenite"), ORE_ILMENITE);
        Registry.register(BuiltinRegistries.CONFIGURED_FEATURE, new Identifier("fracdustry", "ore_deepslate_ilmenite"), ORE_DEEPSLATE_ILMENITE);
        Registry.register(BuiltinRegistries.CONFIGURED_FEATURE, new Identifier("fracdustry", "ore_rare_earth"), ORE_RARE_EARTH);
        Registry.register(BuiltinRegistries.CONFIGURED_FEATURE, new Identifier("fracdustry", "ore_deepslate_rare_earth"), ORE_DEEPSLATE_RARE_EARTH);
        Registry.register(BuiltinRegistries.CONFIGURED_FEATURE, new Identifier("fracdustry", "ore_spodumene"), ORE_SPODUMENE);
        Registry.register(BuiltinRegistries.CONFIGURED_FEATURE, new Identifier("fracdustry", "ore_deepslate_spodumene"), ORE_DEEPSLATE_SPODUMENE);

        /*矿物结构生成注册，FABRIC API*/
        addOreFeature(ORE_BAUXITE_PLACED_ID);
        addOreFeature(ORE_DEEPSLATE_BAUXITE_PLACED_ID);
        addOreFeature(ORE_CASSITERITE_PLACED_ID);
        addOreFeature(ORE_DEEPSLATE_CASSITERITE_PLACED_ID);
        addOreFeature(ORE_ILMENITE_PLACED_ID);
        addOreFeature(ORE_DEEPSLATE_ILMENITE_PLACED_ID);
        addOreFeature(ORE_RARE_EARTH_PLACED_ID);
        addOreFeature(ORE_DEEPSLATE_RARE_EARTH_PLACED_ID);
        addOreFeature(ORE_SPODUMENE_PLACED_ID);
        addOreFeature(ORE_DEEPSLATE_SPODUMENE_PLACED_ID);

        /*placedFeatures注册*/
        Registry.register(BuiltinRegistries.PLACED_FEATURE, ORE_BAUXITE_PLACED_ID, ORE_BAUXITE_PLACED);
        Registry.register(BuiltinRegistries.PLACED_FEATURE, ORE_DEEPSLATE_BAUXITE_PLACED_ID, ORE_DEEPSLATE_BAUXITE_PLACED);
        Registry.register(BuiltinRegistries.PLACED_FEATURE, ORE_CASSITERITE_PLACED_ID, ORE_CASSITERITE_PLACED);
        Registry.register(BuiltinRegistries.PLACED_FEATURE, ORE_DEEPSLATE_CASSITERITE_PLACED_ID, ORE_DEEPSLATE_CASSITERITE_PLACED);
        Registry.register(BuiltinRegistries.PLACED_FEATURE, ORE_ILMENITE_PLACED_ID, ORE_ILMENITE_PLACED);
        Registry.register(BuiltinRegistries.PLACED_FEATURE, ORE_DEEPSLATE_ILMENITE_PLACED_ID, ORE_DEEPSLATE_ILMENITE_PLACED);
        Registry.register(BuiltinRegistries.PLACED_FEATURE, ORE_RARE_EARTH_PLACED_ID, ORE_RARE_EARTH_PLACED);
        Registry.register(BuiltinRegistries.PLACED_FEATURE, ORE_DEEPSLATE_RARE_EARTH_PLACED_ID, ORE_DEEPSLATE_RARE_EARTH_PLACED);
        Registry.register(BuiltinRegistries.PLACED_FEATURE, ORE_SPODUMENE_PLACED_ID, ORE_SPODUMENE_PLACED);
        Registry.register(BuiltinRegistries.PLACED_FEATURE, ORE_DEEPSLATE_SPODUMENE_PLACED_ID, ORE_DEEPSLATE_SPODUMENE_PLACED);

    }

    /*矿物注册懒人函数.jpg*/
    public static void addOreFeature(Identifier oreID){
        BiomeModifications.addFeature(
                BiomeSelectors.foundInOverworld(),
                GenerationStep.Feature.UNDERGROUND_ORES,
                RegistryKey.of(Registry.PLACED_FEATURE_KEY, oreID)
        );
    }

    private static List<PlacementModifier> modifiers(PlacementModifier countModifier, PlacementModifier heightModifier) {
        return List.of(countModifier, SquarePlacementModifier.of(), heightModifier, BiomePlacementModifier.of());
    }

    private static List<PlacementModifier> modifiersWithCount(int count, PlacementModifier heightModfier) {
        return modifiers(CountPlacementModifier.of(count), heightModfier);
    }

    private static List<PlacementModifier> modifiersWithRarity(int chance, PlacementModifier heightModifier) {
        return modifiers(RarityFilterPlacementModifier.of(chance), heightModifier);
    }

}
