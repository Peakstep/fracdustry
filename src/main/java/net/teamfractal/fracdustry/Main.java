package net.teamfractal.fracdustry;

import net.fabricmc.api.ModInitializer;
import net.teamfractal.fracdustry.common.block.init.FDBlocks;
import net.teamfractal.fracdustry.common.item.init.FDItems;
import net.teamfractal.fracdustry.common.world.gen.FDOreGeneration;

import static net.teamfractal.fracdustry.common.itemgroup.init.FDGroupInit.FD_MAIN;

public class Main implements ModInitializer
{
    @Override
    public void onInitialize()
    {
        FDItems.registerItems();
        FDBlocks.registerBlocks();
        FD_MAIN.initialize();
        FDOreGeneration.registerOreGenerations();
    }
}
